class Institution < ApplicationRecord
    has_many :performances
    has_many :courses, through: :performances

    validates_presence_of :name
end
