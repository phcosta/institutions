class Performance < ApplicationRecord
  belongs_to :institution
  belongs_to :course

  validates_presence_of :score, :score_by_course, :average_by_student, :institution_id, :course_id
end
