class Course < ApplicationRecord
    has_many :performances
    has_many :institutions, through: :performances

    validates_presence_of :name
end
