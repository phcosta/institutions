class PerformancesController < ApplicationController
  before_action :set_performance, only: [:show, :update, :destroy]

  def index
    @performances = Performance.order(score: :desc)
    render json: @performances, include: %w{institution course}
  end

  def create
    @performance = Performance.create!(performance_params)
    json_response(@performance, :created)
  end

  def show
    json_response(@performance)
  end

  def update
    @performance.update(performance_params)
    head :no_content
  end

  private

  def set_performance
    @performance = Performance.where(
      institution_id: params[:institution_id],
      course_id: params[:course_id],
    )
  end

  def performance_params
    params.permit(
      :score, :score_by_course, :average_by_student,
      :institution_id, :course_id
    )
  end
end
