class InstitutionsController < ApplicationController
  before_action :set_institution, only: [:show, :update, :destroy]

  def index
    @institutions = Institution.all
    json_response(@institutions)
  end

  def create
    @institution = Institution.create!(institution_params)
    json_response(@institution, :created)
  end

  def show
    json_response(@institution)
  end

  def update
    @institution.update(institution_params)
    head :no_content
  end

  def destroy
    @institution.destroy
    head :no_content
  end

  private

  def institution_params
    params.permit(:name)
  end

  def set_institution
    @institution = Institution.find(params[:id])
  end
end
