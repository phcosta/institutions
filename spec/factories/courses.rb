FactoryGirl.define do
  factory :course do
    name { Faker::Name.unique }
  end
end