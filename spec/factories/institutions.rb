FactoryGirl.define do
  factory :institution do
    name { Faker::Name.unique }
  end
end