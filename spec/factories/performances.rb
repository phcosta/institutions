FactoryGirl.define do
  factory :performance do
    score { Faker::Number.decimal(2) }
    score_by_course { Faker::Number.decimal(2) }
    average_by_student { Faker::Number.decimal(2) }
    institution
    course
  end
end
