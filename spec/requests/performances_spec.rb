require 'rails_helper'

RSpec.describe 'Performances API', type: :request do
  let!(:performances) { create_list(:performance, 10) }
  let(:institution_id) { performances.first.institution_id }
  let(:course_id) { performances.first.course_id }


  describe 'GET /performances' do
    before { get '/performances' }

    it 'returns performances' do
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
      expect(json.first['score']).to be > json.last['score']
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  describe 'GET /performances/institution/:institution_id/course/:course_id' do
    before { get "/performances/institution/#{institution_id}/course/#{course_id}" }

    context 'when the record exists' do
      it 'returns performance' do
        expect(json).not_to be_empty
      end
    end
  end

  describe 'POST /performances' do
    let(:valid_attributes)  { {
      institution_id: institution_id,
      course_id: course_id,
      score: 87.2,
      score_by_course: 45.7,
      average_by_student: 76.8
    } }

    context 'when the request is valid' do
      before { post '/performances', params: valid_attributes }

      it 'creates a performance'do
        expect(json['score']).to eq(87.2)
        expect(json['score_by_course']).to eq(45.7)
        expect(json['average_by_student']).to eq(76.8)
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end
  end

end
