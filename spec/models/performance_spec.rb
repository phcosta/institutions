require 'rails_helper'

RSpec.describe Performance, type: :model do
  it { should belong_to(:institution) }
  it { should belong_to(:course) }

  it { should validate_presence_of(:score) }
  it { should validate_presence_of(:score_by_course) }
  it { should validate_presence_of(:average_by_student) }
  it { should validate_presence_of(:institution_id) }
  it { should validate_presence_of(:course_id) }
end
