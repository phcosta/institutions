# README

To run the application locally do as bellow:

* First, build the Docker image of the application container with all its dependencies:
`docker-compose build`

* Create and migrate database:
`docker-compose run --rm web rails db:create db:migrate`

* To run tests suite:
`docker-compose run --rm web bundle exec rspec`

* Finally, to get the application up and running, issue the command:
`docker-compose run -d`

