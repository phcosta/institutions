FROM ruby:2.4

RUN apt-get update -qq \
    && apt-get install -y build-essential libpq-dev nodejs \
    && rm -rf /var/lib/apt/lists/*

RUN mkdir /usr/src/app
WORKDIR /usr/src/app
COPY Gemfile* ./
RUN bundle install --jobs 20 --retry 5
