Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :institutions
  resources :courses
  resources :performances

  get '/performances/institution/:institution_id/course/:course_id/', to: 'performances#show'
end
