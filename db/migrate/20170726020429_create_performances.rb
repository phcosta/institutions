class CreatePerformances < ActiveRecord::Migration[5.1]
  def change
    create_table :performances do |t|
      t.float :score
      t.float :score_by_course
      t.float :average_by_student
      t.references :institution, foreign_key: true
      t.references :course, foreign_key: true

      t.timestamps
    end
  end
end
